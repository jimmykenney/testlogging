import Log, time


def OneLog():
    Log.LogInfo("Test from OneLog Method")


def AllLogLevels():
    Log.LogDebug("Test Debug Log")
    Log.LogInfo("Test Info Log")
    Log.LogWarn("Test Warning Log")
    Log.LogError("Test Error Log")


def TenLogsTenSec():
    for x in range(1, 11):
        Log.LogWarn("Looping Logs - " + str(x))
        time.sleep(1)

