import logging, pathlib, stackify

import logging.handlers as handlers

logger = logging.getLogger("my_app")
logger.setLevel(logging.INFO)
logDir = str(pathlib.Path(__file__).parent) + "/Logs/app.log"
logHandler = handlers.RotatingFileHandler(logDir, maxBytes=500000, backupCount=10)
formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
logHandler.setLevel(logging.INFO)
logHandler.setFormatter(formatter)
logger.addHandler(logHandler)
stackify_handler = stackify.StackifyHandler(
    application="Test Python Logger",
    environment="Test",
    api_key="6Qk0Na5Ol9By5At5Kj6Km7Ej3Zq9Bd6Vk6Aa7Cd",
)
logger.addHandler(stackify_handler)


def LogDebug(msg):
    logger.debug(msg)


def LogInfo(msg):
    logger.info(msg)


def LogWarn(msg):
    logger.warn(msg)


def LogError(msg):
    logger.error(msg)
