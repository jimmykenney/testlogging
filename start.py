import time, datetime, UploadLogs, Log

UploadLogs.OneLog()
Log.LogInfo("OneLog ran sleeping 5 seconds")
print("OneLog ran sleeping 5 seconds")

time.sleep(5)

UploadLogs.AllLogLevels()
Log.LogInfo("All Log Levels ran, sleeping 5 seconds")
print("All Log Levels ran, sleeping 5 seconds")

time.sleep(5)

UploadLogs.TenLogsTenSec()
Log.LogInfo("Ten logs over ten seconds ran")
print("Ten logs over ten seconds ran")
